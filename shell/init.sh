#!/bin/bash
username=$1;

if [ "X${username}" == 'X' ]; then
  echo "need username";
  exit;
fi


grep "$username" user.txt

if [ $? -eq 0 ]; then
  echo "username exists!";
  exit;
fi

# config variable
smb_config='/etc/samba/smb.conf'
nginx_conf="/etc/nginx/nginx.conf"

user_base_dir="/home/${username}"
user_www_dir="${user_base_dir}/www"
user_conf_dir="${user_base_dir}/conf"


# linux config
useradd $username -m -d /home/${username};
echo $username:$username | chpasswd;

# samba config
echo -e "${username}\n${username}" | smbpasswd -as $username;
sed "s/username/${username}/" conf/smb.conf >> $smb_config

# nginx config
mkdir -p $user_www_dir;
mkdir -p $user_conf_dir;
echo "${username}.example.com works!!" >> $user_www_dir/index.html;
cp ./conf/nginx.conf ${user_conf_dir}/${username}.conf -f
sed -i "s/username/${username}/g" ${user_conf_dir}/${username}.conf
sed -i "/#template/iinclude \"${user_conf_dir}/${username}.conf\";" $nginx_conf;
nginx -s reload;

# docker config
src_dir="/home/${username}/workspace";
dst_dir="/root/"
mkdir -p $src_dir;
id=`docker run -d -P --name ${username} -v $user_base_dir:$dst_dir awde `
port=`docker port ${id:0:12} | awk -F':' '{print $NF}'`

# save user list
echo -e "$username $port" >> user.txt;
echo -e "$username $port";
